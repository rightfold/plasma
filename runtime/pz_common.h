/*
 * Plasma bytecode comon includes
 * vim: ts=4 sw=4 et
 *
 * Copyright (C) 2015 Plasma Team
 * Distributed under the terms of the MIT license, see ../LICENSE.code
 */

#ifndef PZ_COMMON_H
#define PZ_COMMON_H

#include <stdint.h>
#include <stdbool.h>

#include "pz_config.h"

#endif /* ! PZ_COMMON_H */
